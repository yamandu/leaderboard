from unittest import TestCase

from leaderboard import main


class SetupTests(TestCase):

    def setUp(self):
        main.app.all_user_data = {}
        self.client = main.app.test_client()

    def test_get_nonexistent_user(self):
        response = self.client.get("/users/4617/dates/2019-01-01")

        self.assertEqual(404, response.status_code)

    def test_post(self):
        payload = {"user": "4617", "steps": 600, "date": "2019-01-01"}

        response = self.client.post("/data", json=payload)

        self.assertEqual(204, response.status_code)
        self.assertEqual(b'', response.data)

    def test_get_data(self):
        payload = {"user": "4617", "steps": 600, "date": "2019-01-01"}
        self.client.post("/data", json=payload)

        response = self.client.get("/users/4617/dates/2019-01-01")

        expected_contents = {"steps": 600}
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_contents, response.json)
