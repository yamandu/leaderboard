from unittest import TestCase

from leaderboard import main


class LeaderboardTests(TestCase):

    def setUp(self):
        main.app.all_user_data = {}
        self.client = main.app.test_client()

    def test_get_leaderboard_single_user(self):
        payload = {"user": "4617", "steps": 9000, "date": "2019-01-01"}
        self.client.post("/data", json=payload)

        response = self.client.get("/leaderboard/all")

        expected_data = {
            "leaderboard": [
                {"user": "4617", "steps": 9000}
            ]
        }
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_data, response.json)

    def test_get_leaderboard_multi_users(self):
        payloads = [
            {"user": "4616", "steps": 2500, "date": "2019-01-01"},
            {"user": "4616", "steps": 2500, "date": "2019-01-02"},
            {"user": "4616", "steps": 2500, "date": "2019-01-03"},
            {"user": "4617", "steps": 3000, "date": "2019-01-01"},
            {"user": "4617", "steps": 3000, "date": "2019-01-02"},
            {"user": "4617", "steps": 3000, "date": "2019-01-03"},
            {"user": "4618", "steps": 2000, "date": "2019-01-01"},
            {"user": "4618", "steps": 2000, "date": "2019-01-02"},
            {"user": "4618", "steps": 2000, "date": "2019-01-03"},
            {"user": "4619", "steps": 1000, "date": "2019-01-01"},
            {"user": "4619", "steps": 1000, "date": "2019-01-02"},
            {"user": "4619", "steps": 1000, "date": "2019-01-03"}
        ]

        for payload in payloads:
            self.client.post("/data", json=payload)

        response = self.client.get("/leaderboard/all")

        expected_data = {
            "leaderboard": [
                {"user": "4617", "steps": 9000},
                {"user": "4616", "steps": 7500},
                {"user": "4618", "steps": 6000},
                {"user": "4619", "steps": 3000}
            ]
        }
        self.assertEqual(expected_data, response.json)
