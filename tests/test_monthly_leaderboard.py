from unittest import TestCase

from leaderboard import main


class MonthlyLeaderboardTests(TestCase):

    def setUp(self):
        main.app.all_user_data = {}
        self.client = main.app.test_client()

    def test_monthly_leaderboard_single_user_single_month(self):
        payload = {"user": "4617", "steps": 9000, "date": "2019-01-01"}
        self.client.post("/data", json=payload)

        response = self.client.get("/leaderboard/2019-01")

        expected_data = {
            "leaderboard": [
                {
                    "user": "4617",
                    "steps": 9000
                }
            ]
        }
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_data, response.json)

    def test_monthly_leaderboard_single_user_multi_month(self):
        payloads = [
            {"user": "4617", "steps": 9000, "date": "2019-01-01"},
            {"user": "4617", "steps": 2000, "date": "2019-01-02"},
            {"user": "4617", "steps": 10000, "date": "2019-02-01"}
        ]

        for payload in payloads:
            self.client.post("/data", json=payload)

        response = self.client.get("/leaderboard/2019-02")

        expected_data = {
            "leaderboard": [
                {"user": "4617", "steps": 10000}
            ]
        }
        self.assertEqual(expected_data, response.json)

    def test_monthly_leaderboard_multi_user_multi_month(self):
        payloads = [
            {"user": "4616", "steps": 2500, "date": "2019-01-01"},
            {"user": "4616", "steps": 2500, "date": "2019-01-02"},
            {"user": "4616", "steps": 2500, "date": "2019-01-03"},
            {"user": "4617", "steps": 3000, "date": "2019-01-01"},
            {"user": "4617", "steps": 3000, "date": "2019-02-02"},
            {"user": "4617", "steps": 3000, "date": "2019-03-03"},
            {"user": "4618", "steps": 2000, "date": "2019-01-01"},
            {"user": "4618", "steps": 2000, "date": "2019-01-02"},
            {"user": "4618", "steps": 2000, "date": "2019-02-03"},
            {"user": "4619", "steps": 1000, "date": "2019-02-01"},
            {"user": "4619", "steps": 1000, "date": "2019-02-02"},
            {"user": "4619", "steps": 1000, "date": "2019-02-03"}
        ]

        for payload in payloads:
            self.client.post("/data", json=payload)

        response = self.client.get("/leaderboard/2019-01")

        expected_data = {
            "leaderboard": [
                {"user": "4616", "steps": 7500},
                {"user": "4618", "steps": 4000},
                {"user": "4617", "steps": 3000},
                {"user": "4619", "steps": 0}
            ]
        }
        self.assertEqual(expected_data, response.json)
