from unittest import TestCase

from leaderboard import main


class DataDeletionTests(TestCase):

    def setUp(self):
        main.app.all_user_data = {}
        self.client = main.app.test_client()

    def test_delete_data_non_existent(self):
        payload = {"user": "4617", "steps": 9000, "date": "2019-01-01"}
        self.client.post("/data", json=payload)

        response = self.client.delete("/users/4617")

        self.assertEqual(204, response.status_code)
        self.assertEqual(b'', response.data)

    def test_delete_data(self):
        payload = {"user": "4617", "steps": 9000, "date": "2019-01-01"}
        self.client.post("/data", json=payload)

        self.client.delete("/users/4617")

        response = self.client.get("/users/4617/dates/2019-01-01")
        self.assertEqual(404, response.status_code)
