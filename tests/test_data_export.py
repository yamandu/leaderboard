import csv
from io import StringIO
from unittest import TestCase

from leaderboard import main


class DataExportTests(TestCase):

    def setUp(self):
        main.app.all_user_data = {}
        self.client = main.app.test_client()

    def test_export_nonexistent(self):
        response = self.client.get("/users/4617:export")

        self.assertEqual(404, response.status_code)

    def test_export_one_day(self):
        payload = {"user": "4617", "steps": 9000, "date": "2019-01-01"}
        self.client.post("/data", json=payload)

        response = self.client.get("/users/4617:export")

        self.assertEqual(200, response.status_code)
        self.assertIn('text/csv', response.headers['content-type'])
        self.assertIn('filename=4617.csv', response.headers['content-disposition'])

        buffer = StringIO()
        writer = csv.DictWriter(buffer, fieldnames=['date', 'steps'])
        writer.writeheader()
        writer.writerow({"date": "2019-01-01", "steps": 9000})

        self.assertEqual(buffer.getvalue().encode('utf-8'), response.data)

    def test_export_multi_day(self):
        payloads = [
            {"user": "4617", "steps": 100, "date": "2019-02-01"},
            {"user": "4617", "steps": 200, "date": "2019-03-01"},
            {"user": "4617", "steps": 300, "date": "2019-04-01"},
            {"user": "4617", "steps": 400, "date": "2019-01-01"},
            {"user": "4617", "steps": 500, "date": "2019-01-02"},
            {"user": "4617", "steps": 600, "date": "2019-01-03"}
        ]

        for payload in payloads:
            self.client.post("/data", json=payload)

        response = self.client.get("/users/4617:export")

        buffer = StringIO()
        writer = csv.DictWriter(buffer, fieldnames=['date', 'steps'])
        writer.writeheader()
        writer.writerow({"date": "2019-01-01", "steps": 400})
        writer.writerow({"date": "2019-01-02", "steps": 500})
        writer.writerow({"date": "2019-01-03", "steps": 600})
        writer.writerow({"date": "2019-02-01", "steps": 100})
        writer.writerow({"date": "2019-03-01", "steps": 200})
        writer.writerow({"date": "2019-04-01", "steps": 300})

        self.assertEqual(buffer.getvalue().encode('utf-8'), response.data)
