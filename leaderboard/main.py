from flask import Flask, request, jsonify
from werkzeug.exceptions import NotFound

app = Flask(__name__)
app.all_user_data = {}


@app.route("/users/<user_id>/dates/<date>", methods=['GET'])
def get_data(user_id, date):
    try:
        user_data = app.all_user_data[user_id]
        steps = user_data[date]
        return jsonify({"steps": steps})
    except KeyError:
        raise NotFound


@app.route("/data", methods=['POST'])
def post_data():
    data = request.json
    user_id = data['user']
    date = data['date']
    steps = data['steps']

    app.all_user_data.setdefault(user_id, {})
    app.all_user_data[user_id][date] = steps

    return '', 204


if __name__ == "__main__":
    app.run(port=8080)
