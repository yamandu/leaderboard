# BioIntellisense Leaderboard Interview Problem

This repository is a test problem for BioIntellisense Python engineer candidates.  It is intended to verify a candidate's basic problem-solving ability, programming technique, and knowledge of the Python language.  You may make use of any online resources during this exercise.  You may not use anyone else's code, or use code which was written prior to the start of the exercise.

## Overview

This repository implements a simple RESTful API, written using [Flask](http://flask.pocoo.org/docs/1.0/). There are several feature requests, or objectives, described below. Your goal is to implement as many of the objectives as possible in the time provided.

### Initial API

The existing API is a stripped-down version of one of the problems that BioIntellisense has worked on in the past - a step-tracking web application.  It has two endpoints:

#### GET `/users/<user_id>/dates/<date>`

Sample response:

```json
{
  "steps": 600
}
```

If the user ID cannot be found or the user has no data on that date, return a 404.

#### POST `/data`

This endpoint accepts data in the format:

```json
{
  "user": "4617",
  "date": "2019-01-01",
  "steps": 600
}
```

Data posted to this endpoint is saved by the application and is available to the GET endpoint.

## Getting Started

### Python Version

This repository requires Python 3 or greater.  If you do not have Python 3 installed, install it before continuing.

### Dependencies

Create a new virtual environment and install the dependencies from requirements.txt.

### Run Setup Tests

Run the unit tests in `tests/test_setup.py` to verify that your development environment is set up properly.

## Objectives

Each objective has a test file associated with it.  In order to pass an objective, the test file must pass.  Objectives can be worked on in any order, but may make more sense if worked on sequentially.

### All-Time Leaderboard

A new feature request has come in - a steps leaderboard.  To achieve this, we must create a new endpoint that lists out all users of the application in order of their total step counts (descending).  The spec of this endpoint is as follows:

#### GET /leaderboard/all

Sample response:

```json
{
  "leaderboard": [
    {
      "user": "4617",
      "steps": 100000
    },
    {
      "user": "7164",
      "steps": 90000
    },
    ...
  ]
}
```
  
The tests for this feature are in `tests/test_leaderboard.py`.  Add a new endpoint to the API that causes all of the tests in this test file to pass.

### Monthly Leaderboard

After the great success of the all-time leaderboard, the product team has determined that users would really like the ability to view monthly leaderboards.  To achieve this, we must create a new endpoint that lists out all users of the application, in order of their total step counts for that month (descending).  The spec of this endpoint is as follows:

#### GET `/leaderboard/months/<month>`

`month` is formatted as `yyyy-MM`.  Example: `2019-01`.

Sample response:

```json
{
  "leaderboard": [
    {
      "user": "4617",
      "steps": 20000
    },
    {
      "user": "7614",
      "steps": 19000
    },
    ...
  ]
}
```

The tests for this feature are in `tests/test_monthly_leaderboard.py`.  Add a new endpoint to the API that causes all of the tests in this test file to pass.

### Data Export

A fitness data aggregation app has recently partnered with us.  This contract requires that existing members of our trackers who want to track their data in the partner app be able to download their steps data and upload it to the partner's site.  To achieve this, we must create a new endpoint that allows users to export their steps data to a CSV.  The spec of this endpoint is as follows:

#### GET `/users/<user_id>:export`

which will return an attachment titled `<user_id>.csv` (replacing user_id with their actual user ID) with the following schema:

```csv
date,steps
2019-01-01,600
2019-01-02,800
2019-01-03,700
```

Rows should be ordered by `date`.

The tests for this feature are in `tests/test_data_export.py`.  Add a new endpoint to the API that causes all of the tests in this test file to pass.

### Data Deletion

The company would like to enter the European Union's market.  However, the EU's GDPR states that users have the right to be forgotten - if they request it, then the company must delete all of their personal data.  To comply with this, we must create a new endpoint which allows users to delete their steps data.

#### DELETE `/users/<user_id>`

This endpoint should return a 204 response code.  Any requests for this user's data should now return empty, and the user should not show up in any leaderboards.

The tests for this feature are in `tests/test_data_deletion.py`.  Add a new endpoint to the API that causes all of the tests in this test file to pass.
